import React from "react";

export default function NewsletterForm() {
    return (
        <p>
            Subscribe to Now You See It posts for free by <a href="https://www.patreon.com/signup?ru=%2Fnowyouseeit" target="__blank">following me on Patreon. </a>
        </p>
    )
}