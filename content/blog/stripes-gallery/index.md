---
title: "Stripes In Movies: The Master List"
date: 2021-03-22T12:00:00Z
titleImage: straitjacket.jpg
---
A while back, I made [a video](https://www.youtube.com/watch?v=Y1U4YkNkoG0) about the meaning of stripes in movies. At the end, I invited whoever watched it to submit their own, so together, we could create "the first compiled collection of stripes in film available on the internet." 

After going through hundreds of pictures you all submitted over the years, I turned them into a gallery of stripes at [nowyouseeit.com/stripes](https://www.nowyouseeit.com/stripes). Together, we made something never before seen in academia or on the internet: a living document of the power of stripes in film. Some of you had amazing insights from movies I had never even thought of!

As promised, if you submitted an image that made it onto the website, I added your name to give you credit (check by clicking on the image). Shoutout to Laura Wanco for finding the coolest stripe of all from _Strait Jacket_ (1964), which is the title image of this post.

I'm blown away by the number of people who contributed incredible examples. Please keep submitting them! I'll continue updating the gallery. And if you have any feedback for the site/gallery, comment on this post on our [subreddit](https://reddit.com/r/nowyouseeit).

If you're receiving this as an email, that's because you signed up for the newsletter at the now-defunct nowyouseeit.blog. Feel free to unsubscribe below, or stick around for upcoming videos and articles from Now You See It.